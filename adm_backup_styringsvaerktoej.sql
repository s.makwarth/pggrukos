BEGIN;

	CREATE SCHEMA IF NOT EXISTS backup_styr;

	DO
	$$
		BEGIN
			EXECUTE format('DROP TABLE IF EXISTS backup_styr.%I', 'backup_styringsvaerktoej_' || to_char(current_timestamp,'DD_HH24'));
		END;
	$$ LANGUAGE plpgsql;
   
	DO
	$$
		BEGIN
			EXECUTE format('CREATE TABLE backup_styr.%I AS SELECT * FROM gvkort.styringsvaerktoej', 'backup_styringsvaerktoej_' || to_char(current_timestamp,'DD_HH24'));
		END;
	$$ LANGUAGE plpgsql;

COMMIT;
	